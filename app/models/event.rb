class Event < ApplicationRecord

  KINDS = [
    OPENING = "opening",
    APPOINTMENT = "appointment"
  ].freeze

  SLOT_DURATION = 30.minutes.freeze

  before_save :set_weekday

  validates_inclusion_of :kind, in: KINDS
  validates_presence_of :starts_at, :ends_at
  validate :ends_date_cannot_be_before_start_date

  scope :openings_on, -> (date) {
    where(kind: OPENING).where("DATE(starts_at) = ?", date)
  }

  scope :appointments_on, -> (date) {
    where(kind: APPOINTMENT).where("DATE(starts_at) = ?", date)
  }

  scope :recurring_openings_on, -> (date) {
    where(kind: OPENING)
    .where(weekly_recurring: true)
    .where(week_day: date.wday)
  }

  def self.availabilities(start_date)
    end_date = start_date + 7.days

    (start_date...end_date).map { |day| day_availabilities_response(day) }
  end

  def slots
    (starts_at.to_i...ends_at.to_i).step(SLOT_DURATION.to_i).map do |epoch_slot|
      Time.at(epoch_slot).utc.strftime("%l:%M").strip
    end
  end

  private

  def self.day_availabilities_response(date)
    {
      date: date.to_date,
      slots: day_availabilities(date)
    }
  end

  def self.day_availabilities(date)
    day_openings(date) - day_appointments(date)
  end

  def self.day_openings(date)
    specific_openings = Event.openings_on(date).map(&:slots).flatten

    recurring_openings = Event.recurring_openings_on(date).map(&:slots).flatten

    specific_openings + recurring_openings
  end

  def self.day_appointments(date)
    Event.appointments_on(date).map(&:slots).flatten
  end


  def ends_date_cannot_be_before_start_date
    return unless starts_at.present? && ends_at.present?

    errors.add(:ends_at, "cannot be before starts_at") if ends_at < starts_at
  end

  def set_weekday
    return true unless weekly_recurring

    self.week_day = starts_at.wday

    true
  end

end
