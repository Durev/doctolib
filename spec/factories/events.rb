FactoryBot.define do
  factory :event do
    kind { Event::KINDS.sample }
    starts_at { Time.now }
    ends_at { Time.now + rand(10).days }
  end
end
