RSpec.describe Event, type: :model do
  let(:event) { FactoryBot.build(:event) }

  it "has a valid factory" do
    expect(event).to be_valid
  end

  describe "@kind" do
    it { is_expected.to validate_inclusion_of(:kind).in_array(Event::KINDS) }
  end

  describe "@starts_at" do
    it { is_expected.to validate_presence_of(:starts_at) }
  end

  describe "@ends_at" do
    it { is_expected.to validate_presence_of(:ends_at) }

    it "is after start date" do
      event = FactoryBot.build(:event, starts_at: Time.now, ends_at: 2.days.ago)
      event.valid?
      expect(event.errors[:ends_at].size).to eq(1)
    end
  end
end
