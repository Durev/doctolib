class AddWeekDayToEvents < ActiveRecord::Migration[5.2]
  def change
    add_column(:events, :week_day, :integer)
  end
end
