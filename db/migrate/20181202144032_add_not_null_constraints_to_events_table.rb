class AddNotNullConstraintsToEventsTable < ActiveRecord::Migration[5.2]
  def change
    change_column_null(:events, :starts_at, false)
    change_column_null(:events, :ends_at, false)
    change_column_null(:events, :kind, false)
  end
end
